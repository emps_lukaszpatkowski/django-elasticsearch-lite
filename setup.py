import os
from setuptools import setup

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()
with open('requirements.txt', 'r') as x:
    REQUIREMENTS = filter(None, x.read().split('\n'))

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-elasticsearch-lite',
    version='0.1.4',
    packages=['elasticsearch_lite'],
    include_package_data=True,
    license='BSD License',  # example license
    description='Simple elasticsearch indexing for your django models',
    long_description=README,
    install_requires=REQUIREMENTS,
    author='Kamil Strzelecki',
    author_email='kamil.strzelecki@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Indexing/Search',
    ],
)
