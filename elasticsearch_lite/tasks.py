from __future__ import absolute_import
from celery import shared_task
from celery.utils.log import get_task_logger

from django.db.models.loading import get_model
from django.db import connection


logger = get_task_logger(__name__)


@shared_task(bind=True, default_retry_delay=2 * 60, max_retries=10,
             acks_late=True)
def model_operation(self, operation, app_label, model_name, obj_id, args=None):
    logger.debug("Model_operation with: %s %s %s %s %s", operation, app_label,
                 model_name, obj_id, args)
    model = get_model(app_label, model_name)
    args = args if args else []
    try:
        instance = model.objects.get(pk=obj_id)
        getattr(instance, operation)(*args)
    except Exception as e:
        logger.exception("model_operation failed with: %s %s %s %s %s",
                         operation, app_label, model_name, obj_id, args)
        self.retry(exc=e)

    connection.close()
