# -*- coding: utf-8 -*-
import json
from optparse import make_option

from django.core.management.base import BaseCommand

from elasticsearch_lite.mixins import get_registered


class Command(BaseCommand):
    help = "List and install Elasticsearch mapings for models"
    args = "<app_name>"
    option_list = BaseCommand.option_list + (
        make_option('--show-local',
                    action='store_true',
                    dest='show',
                    default=True,
                    help='show mappings defined for models'),
        make_option('--install',
                    action='store_true',
                    dest='install',
                    default=False,
                    help='install mappings defined for model'),
    )

    def handle(self, *args, **options):
        app_label = args[0] if args else None
        models = get_registered(app_label)
        with_mappings = [m for m in models if m.es_get_defined_mappings()]
        for model in with_mappings:
            self.stdout.write('Model %s.%s' % (model._meta.app_label,
                                               model._meta.module_name))
            if options['install']:
                model.es_put_mappings()
            elif options['show']:
                mappings = model.es_get_defined_mappings()
                self.stdout.write(json.dumps(mappings))
